"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// ---------------------------------------------DETRUCTORING---------------------------------------------
const array1 = [1, "2", 3, "4"];
const [item_1, item_2, , , item5] = array1;
// console.log("item1", item1);
// console.log("item2", item2);
const person = {
    name: 'Nam',
    age: 21,
    job: 'dev'
};
const { job: j, age: a } = person;
// console.log(j);
// console.log(a);
const ex1Question1 = () => {
    var date = [2020, 12, 8];
    const [year, month, day] = date;
    console.log('Year: ', year);
    console.log('Month:', month);
    console.log('Day:', day);
};
// ex1Question1();
// ------------------------------------------------OPERATOR-----------------------------------------
const [item1, item2, ...rest] = array1;
// console.log('rest: ', rest);
const array2 = [...array1];
// console.log('Array 2: ', array2);
const cold = ['autumn', 'winter'];
const warm = ['spring', 'summer'];
const seasons = [...cold, ...warm];
// console.log('Seasons: ', seasons);
// -----------------------------------------------TEMPLATE STRING---------------------------------------
const string1 = 'My name is';
const string2 = 'Hello';
// console.log(`${string2}, ${string1} Nguyet.`);
const fullName = 'Do Thi Minh Nguyet';
const age = 21;
// console.log(`Tôi tên là ${fullName}, năm nay ${age} tuổi.`);
// --------------------------------------------ARROW FUNCTION---------------------------------------
const printHello = (name) => {
    console.log(`Hello, ${name}`);
};
// printHello('Nguyet');
const question2a = (a, b) => a + b + 100;
// console.log(question2a(22,22));
const question2b = (a, b) => {
    let chuck = 42;
    return a + b + chuck;
};
// console.log(question2b(1,2));
const bob = (a) => a + 100;
// console.log(bob(1));
//----------------------------------------PARAMETER---------------------------------------
const add = (...args) => args.reduce((prev, current) => {
    return prev + current;
}, 0);
// console.log(add(1,2,3,4,5,6));
const multiply = (a, b) => {
    if (b)
        return a * b;
    else
        return a;
};
console.log(multiply(5));
//---------------------------ASYNCHONOUS-------------------------//
const function1 = (a, callbackfn) => {
    console.log('run with ' + a);
    callbackfn();
};
function1('fn1', () => {
    console.log('callback function');
});
//-----------------------------------PROMISE--------------------------------------
let promise = new Promise(function (resolve, reject) {
    resolve('Success');
});
const multiply2 = (a, b) => {
    promise.then(result => {
        if (b)
            result = a * b;
        else
            result = a;
        console.log(result);
    });
};
multiply2(2, 4);
//---------------------------------------CALLBACK----------------------------------------
function getListAccount() {
    // Lấy ra danh sách Account
    // Gán giá trị lấy được cho accountList
    $.ajax({
        url: "https://643d4c71f0ec48ce90586caf.mockapi.io/Account",
        type: 'GET',
        contentType: 'application/json',
        // data: JSON.stringify(request),
        error: function (err) {
            // Hành động khi api bị lỗi
            console.log(err);
            alert(err.responseJSON);
        },
        success: function (data) {
            // Hành động khi thành công
            console.log(data);
        }
    });
}
