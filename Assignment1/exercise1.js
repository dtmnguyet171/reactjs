"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exercise1 = exports.GroupAccount = exports.Group = exports.Account = exports.Position = exports.Department = void 0;
// ------------------------------------------ DataType -------------------------------------------------
var Department = exports.Department = /** @class */ (function () {
    function Department(departmentName) {
        this.departmentId = ++Department.counter;
        this.departmentName = departmentName;
    }
    Department.prototype.getDepartmentName = function () {
        return this.departmentName;
    };
    Department.prototype.setDepartmentName = function (value) {
        this.departmentName = value;
    };
    Department.prototype.printInformation = function () {
        console.log("Department ID: " + this.departmentId);
        console.log("Department Name: " + this.departmentName);
    };
    Department.counter = 0;
    return Department;
}());
var Position = exports.Position = /** @class */ (function () {
    function Position(positionName) {
        this.positionId = ++Position.counter;
        this.positionName = positionName;
    }
    Position.prototype.getPositionName = function () {
        return this.positionName;
    };
    Position.prototype.setPositionName = function (value) {
        this.positionName = value;
    };
    Position.prototype.printInformation = function () {
        console.log("Position ID: " + this.positionId);
        console.log("Position Name: " + this.positionName);
    };
    Position.counter = 0;
    return Position;
}());
var Account = exports.Account = /** @class */ (function () {
    function Account(email, username, fullName, departmentId, positionId) {
        this.accountId = ++Account.counter;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.departmentId = departmentId;
        this.positionId = positionId;
        this.createDate = new Date();
    }
    Account.prototype.getEmail = function () {
        return this.email;
    };
    Account.prototype.getUsername = function () {
        return this.username;
    };
    Account.prototype.getFullName = function () {
        return this.fullName;
    };
    Account.prototype.getDepartmentId = function () {
        return this.departmentId;
    };
    Account.prototype.getPositionId = function () {
        return this.positionId;
    };
    Account.prototype.getCreateDate = function () {
        return this.createDate;
    };
    Account.prototype.setEmail = function (value) {
        this.email = value;
    };
    Account.prototype.setUsername = function (value) {
        this.username = value;
    };
    Account.prototype.setFullName = function (value) {
        this.fullName = value;
    };
    Account.prototype.setDepartmentId = function (value) {
        this.departmentId = value;
    };
    Account.prototype.setPositionId = function (value) {
        this.positionId = value;
    };
    Account.prototype.setCreateDate = function (value) {
        this.createDate = value;
    };
    Account.prototype.printInformation = function () {
        console.log("Account Id: " + this.accountId);
        console.log("Username: " + this.username);
        console.log("Full Name: " + this.fullName);
        console.log("Department Id: " + this.departmentId);
        console.log("Position Id: " + this.positionId);
        console.log("Create Date: " + this.createDate);
    };
    Account.counter = 0;
    return Account;
}());
var Group = exports.Group = /** @class */ (function () {
    function Group(groupName, creatorId) {
        this.groupId = ++Group.counter;
        this.groupName = groupName;
        this.creatorId = creatorId;
        this.createDate = new Date();
    }
    Group.prototype.getGroupName = function () {
        return this.groupName;
    };
    Group.prototype.getCreatorId = function () {
        return this.creatorId;
    };
    Group.prototype.getCreateDate = function () {
        return this.createDate;
    };
    Group.prototype.setGroupName = function (value) {
        this.groupName = value;
    };
    Group.prototype.setCreatorId = function (value) {
        this.creatorId = value;
    };
    Group.prototype.setCreateDate = function (value) {
        this.createDate = value;
    };
    Group.prototype.printInformation = function () {
        console.log("Group Id: " + this.groupId);
        console.log("Group Name: " + this.groupName);
        console.log("Creator Id: " + this.creatorId);
        console.log("Create Date: " + this.createDate);
    };
    Group.counter = 0;
    return Group;
}());
var GroupAccount = /** @class */ (function () {
    function GroupAccount(groupId, accountId) {
        this.groupId = groupId;
        this.accountId = accountId;
        this.joinDate = new Date();
    }
    GroupAccount.prototype.getGroupId = function () {
        return this.groupId;
    };
    GroupAccount.prototype.getAccountId = function () {
        return this.accountId;
    };
    GroupAccount.prototype.getJoinDate = function () {
        return this.joinDate;
    };
    GroupAccount.prototype.setGroupId = function (value) {
        this.groupId = value;
    };
    GroupAccount.prototype.setAccountId = function (value) {
        this.accountId = value;
    };
    GroupAccount.prototype.setJoinDate = function (value) {
        this.joinDate = value;
    };
    GroupAccount.prototype.printInformation = function () {
        console.log("Group Id: " + this.groupId);
        console.log("Account Id: " + this.accountId);
        console.log("Join Date: " + this.joinDate);
    };
    return GroupAccount;
}());
exports.GroupAccount = GroupAccount;
// ------------------------------------------- String ---------------------------------------------
var readline = require("readline");
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var Exercise1 = /** @class */ (function () {
    function Exercise1() {
    }
    Exercise1.prototype.question1 = function () {
        rl.question('Nhập vào chuỗi ký tự: ', function (string) {
            console.log("số ký tự của chuỗi " + string + " là: " + string.length);
            rl.close();
        });
    };
    Exercise1.prototype.question2 = function () {
        rl.question('Nhập vào chuỗi thứ nhất: ', function (s1) {
            rl.question('Nhập vào chuỗi thứ hai: ', function (s2) {
                console.log(s1.concat(s2));
                rl.close();
            });
        });
    };
    Exercise1.prototype.question3 = function () {
        rl.question('Nhập vào tên: ', function (name) {
            var firstChar = name.charAt(0).toUpperCase();
            var remain = name.substring(1);
            console.log("Tên sau khi định dạng là: " + firstChar.concat(remain));
            rl.close();
        });
    };
    Exercise1.prototype.question4 = function () {
        rl.question('Nhập vào tên: ', function (name) {
            for (var i = 0; i < name.length; i++) {
                console.log("Ký tự thứ " + (i + 1) + " là: " + name.charAt(i).toUpperCase());
            }
            rl.close();
        });
    };
    Exercise1.prototype.question5 = function () {
        rl.question('Nhập vào họ: ', function (firstName) {
            rl.question('Nhập vào tên: ', function (lastName) {
                console.log('Họ và tên: ' + firstName.concat(' ').concat(lastName));
                rl.close();
            });
        });
    };
    Exercise1.prototype.question6 = function () {
        rl.question('Nhập vào họ tên: ', function (fullName) {
            var words = fullName.split(' ');
            var firstName = words[0];
            var lastName = words[words.length - 1];
            var middleName = '';
            for (var i = 1; i < (words.length - 1); i++) {
                middleName += words[i] + ' ';
            }
            console.log('Họ là: ' + firstName);
            console.log('Tên đệm là: ' + middleName);
            console.log('Tên là: ' + lastName);
            rl.close();
        });
    };
    Exercise1.prototype.question7 = function () {
        rl.question('Nhập vào họ tên: ', function (fullName) {
            fullName = fullName.trim(); //bỏ khoảng trắng 2 đầu
            fullName = fullName.replace(/\s+/g, ' '); //bỏ khoảng trắng ở giữa
            var words = fullName.split(' ');
            var result = ' ';
            for (var i = 0; i < words.length; i++) {
                var firstChar = words[i].charAt(0).toUpperCase(); //viết hoa chữ đầu tiên
                var remain = words[i].substring(1).toLowerCase();
                var word = firstChar.concat(remain); //ghép chữ cái đầu với phần còn lại
                result += word + ' '; //ghép các từ
            }
            console.log('Họ tên sau khi chuẩn hóa là: ' + result);
            rl.close();
        });
    };
    Exercise1.prototype.question8 = function () {
        var groupName = new Array('Java', 'JavaScript', '.NET', 'hihiJava');
        for (var i = 0; i < groupName.length; i++) {
            if (groupName[i].search('Java') != -1) {
                console.log(groupName[i]);
            }
        }
    };
    Exercise1.prototype.question9 = function () {
        var groupName = new Array('Java', 'JavaScript', '.NET');
        for (var i = 0; i < groupName.length; i++) {
            if (groupName[i] == 'Java') {
                console.log(groupName[i]);
            }
        }
    };
    Exercise1.prototype.question10 = function (string1, string2) {
        var reverse = '';
        var string1 = string1;
        var string2 = string2;
        for (var i = (string1.length - 1); i >= 0; i--) {
            var char = string1.charAt(i);
            reverse += char;
        }
        if (string2 == reverse) {
            console.log('Đây là chuỗi đảo ngược.');
        }
        else {
            console.log('Đây không phải là chuỗi đảo ngược.');
        }
    };
    Exercise1.prototype.question11 = function (string) {
        var string = string;
        var count = 0;
        for (var i = 0; i < string.length; i++) {
            if (string.charAt(i).search('a') != -1) {
                count++;
            }
        }
        console.log("Số lần xuất hiện ký tự a là: " + count);
    };
    Exercise1.prototype.question12 = function (string) {
        var reverse = '';
        var string = string;
        for (var i = (string.length - 1); i >= 0; i--) {
            var char = string.charAt(i);
            reverse += char;
        }
        console.log("Chuỗi đảo ngược là: " + reverse);
    };
    Exercise1.prototype.question13 = function (string) {
        var string = string;
        var regex = /[0-9]/gi;
        var result;
        for (var i = 0; i < string.length; i++) {
            if (string.charAt(i).search(regex) != -1) {
                result = false;
            }
            else {
                result = true;
            }
        }
        console.log(result);
    };
    Exercise1.prototype.question14 = function (string) {
        var string = string;
        console.log(string.replace('e', '*'));
    };
    Exercise1.prototype.question15 = function (string) {
        var words = string.split(' ');
        var stringReverse = '';
        for (var i = (words.length - 1); i >= 0; i--) {
            var word = words[i];
            stringReverse += word + ' ';
        }
        console.log(words);
        console.log("Chuỗi đảo ngược: " + stringReverse);
    };
    Exercise1.prototype.question16 = function (string, n) {
        var string = string;
        var n = n;
        if (string.length % n == 0) {
            for (var i = 0; i < string.length; i += n) {
                console.log(string.substring(i, (i + n)));
            }
        }
        else {
            console.log("KO");
        }
    };
    return Exercise1;
}());
exports.Exercise1 = Exercise1;
