"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exercise4 = void 0;
var Exercise4 = /** @class */ (function () {
    function Exercise4() {
    }
    Exercise4.prototype.question1 = function () {
    };
    Exercise4.prototype.question2 = function (myName) {
        var a = function printName(myName) {
            console.log(myName);
        };
        a(myName);
    };
    Exercise4.prototype.question3 = function (firstName, lastName) {
        function getGreeting(firstName, lastName) {
            function greeting() {
                return firstName + ' ' + lastName;
            }
            return greeting;
        }
        var result = getGreeting(firstName, lastName);
        console.log(result());
    };
    Exercise4.prototype.question4 = function () {
        var timer = function (i) {
            setTimeout(function timer() {
                console.log(i);
            }, i * 1000);
        };
        for (var i = 1; i <= 5; i++) {
            timer(i);
        }
    };
    Exercise4.prototype.question5 = function (x, y) {
        function makeExponentiation(x, y) {
            return Math.pow(x, y);
        }
        var result = makeExponentiation(x, y);
        if (y == 2) {
            console.log('Bình phương của ' + x + ' là: ' + result);
        }
        else if (y == 0.5) {
            console.log('Căn bậc 2 của ' + x + ' là: ' + result);
        }
    };
    return Exercise4;
}());
exports.Exercise4 = Exercise4;
