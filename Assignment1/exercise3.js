"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exercise3 = void 0;
var Exercise3 = /** @class */ (function () {
    function Exercise3() {
    }
    Exercise3.prototype.question1 = function () {
        var array = ['a', 'b', 'c', 'd', 'e'];
        console.log('In array theo index');
        for (var i = 0; i < array.length; i++) {
            console.log(array[i]);
        }
        console.log('In array theo value');
        array.forEach(function (element) {
            console.log(element);
        });
    };
    return Exercise3;
}());
exports.Exercise3 = Exercise3;
