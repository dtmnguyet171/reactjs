export class Exercise2 {
    public question1() {
        var salary1 = 5240.5;
        var salary2 = 10970.055;

        var salaryRound1 = salary1.toFixed();
        var salaryRound2= salary2.toFixed();

        console.log('Lương Account 1 sau khi làm tròn là: ' + salaryRound1);
        console.log('Lương Account 2 sau khi làm tròn là: ' + salaryRound2);
    }

    public question2() {
        var randomNumber: number = Math.random() * 89999 + 10000;
        var result: number = Math.floor(randomNumber);
        console.log('Số ngẫu nhiên là: ' + result);
        return result;
    }

    public question3() {
        var randomNumber: number = this.question2();
        console.log('Hai số cuối là: ' + randomNumber%100);
    }
}