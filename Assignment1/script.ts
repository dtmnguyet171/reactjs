// ------------------------------------------ DataType -------------------------------------------------
import { Department } from "./exercise1";
import { Position } from "./exercise1";
import { Account } from "./exercise1";
import { Group } from "./exercise1";
import { GroupAccount } from "./exercise1";

let department1: Department = new Department("Sale");
let department2: Department = new Department("Marketing");

let position1: Position = new Position("Dev");
let position2: Position = new Position("Test");
let position3: Position = new Position("Scrum Master");
let position4: Position = new Position("PM");

let account1: Account = new Account("account1@gmail.com", "account1", "Account 1", department1, position2);
let account2: Account = new Account("account2@gmail.com", "account2", "Account 2", department2, position4);
let account3: Account = new Account("account3@gmail.com", "account3", "Account 3", department1, position1);

let group1: Group = new Group("Group 1", account2);
let group2: Group = new Group("Group 2", account1);
let group3: Group = new Group("Group 3", account3);

let groupAccount1: GroupAccount = new GroupAccount(group1, account2);
let groupAccount2: GroupAccount = new GroupAccount(group3, account2);
let groupAccount3: GroupAccount = new GroupAccount(group1, account3);

// ------------------------------------------- String ---------------------------------------------
import { Exercise1 } from "./exercise1";
let ex1: Exercise1 = new Exercise1;
// ex1.question1();
// ex1.question2();
// ex1.question3();
// ex1.question4();
// ex1.question5();
// ex1.question6();
// ex1.question7();
// ex1.question8();
// ex1.question9();
// ex1.question10('OK', 'KOK');
// ex1.question11('anh Tu Duc Phuong Nam');
// ex1.question12('Do Thi Minh Nguyet');
// ex1.question13('Nguyet123')
// ex1.question14('Nguyet');
// ex1.question15('Do Thi Minh Nguyet');
// ex1.question16('Do Thi Minh Nguyet', 4);

// ----------------------------------------- Datatype Casting -----------------------------------------
import { Exercise2 } from "./exercise2";
let ex2 = new Exercise2;
// ex2.question1();
// ex2.question2();
// ex2.question3();

// ------------------------------------------ Flow Control & Operator -------------------------------------
import { Exercise3 } from "./exercise3";
let ex3 = new Exercise3;
// ex3.question1();

// ------------------------------------------ Scope, Closure --------------------------------------
import { Exercise4 } from "./exercise4";
let ex4 = new Exercise4;
// ex4.question2('Do Thi Minh Nguyet');
// ex4.question3('Do', 'Nguyet');
// ex4.question4();
// ex4.question5(9, 0.5);

