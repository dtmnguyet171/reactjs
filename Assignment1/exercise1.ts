// ------------------------------------------ DataType -------------------------------------------------
export class Department {
    private static counter: number = 0;

    private departmentId: number;
    private departmentName: string;

    constructor(departmentName: string) {
        this.departmentId = ++Department.counter;
        this.departmentName = departmentName;
    }

    public getDepartmentName(): string{
        return this.departmentName;
    }

    public setDepartmentName(value: string) {
        this.departmentName = value;
    }

    public printInformation() {
        console.log("Department ID: " + this.departmentId);
        console.log("Department Name: " + this.departmentName);
    }
}

export class Position {
    private static counter: number = 0;

    private positionId: number;
    private positionName: string;

    constructor(positionName: string) {
        this.positionId = ++Position.counter;
        this.positionName = positionName;
    }

    public getPositionName(): string {
        return this.positionName;
    }

    public setPositionName(value: string) {
        this.positionName = value;
    }

    public printInformation() {
        console.log("Position ID: " + this.positionId);
        console.log("Position Name: " + this.positionName);
    }
}

export class Account {
    private static counter: number = 0;

    private accountId: number;
    private email: string;
    private username: string;
    private fullName: string;
    private departmentId: Department;
    private positionId: Position;
    private createDate: Date;

    constructor(email: string, username: string, fullName: string, departmentId: Department, positionId: Position) {
        this.accountId = ++Account.counter;
        this.email = email;
        this.username = username;
        this.fullName = fullName;
        this.departmentId = departmentId;
        this.positionId = positionId;
        this.createDate = new Date();
    }
    public getEmail(): string {
        return this.email;
    }

    public getUsername(): string {
        return this.username;
    }

    public getFullName(): string {
        return this.fullName;
    }

    public getDepartmentId(): Department {
        return this.departmentId;
    }

    public getPositionId(): Position {
        return this.positionId;
    }

    public getCreateDate(): Date {
        return this.createDate;
    }

    public setEmail(value: string) {
        this.email = value;
    }

    public setUsername(value: string) {
        this.username = value;
    }

    public setFullName(value: string) {
        this.fullName = value;
    }

    public setDepartmentId(value: Department) {
        this.departmentId = value;
    }

    public setPositionId(value: Position) {
        this.positionId = value;
    }

    public setCreateDate(value: Date) {
        this.createDate = value;
    }

    public printInformation() {
        console.log("Account Id: " + this.accountId);
        console.log("Username: " + this.username);
        console.log("Full Name: " + this.fullName);
        console.log("Department Id: " + this.departmentId);
        console.log("Position Id: " + this.positionId);
        console.log("Create Date: " + this.createDate);
    }
}

export class Group {
    private static counter: number = 0;

    private groupId: number;
    private groupName: string;
    private creatorId: Account;
    private createDate: Date;

    constructor(groupName: string, creatorId: Account) {
        this.groupId = ++Group.counter;
        this.groupName = groupName;
        this.creatorId = creatorId;
        this.createDate = new Date();
    }

    public getGroupName(): string {
        return this.groupName;
    }

    public getCreatorId(): Account {
        return this.creatorId;
    }

    public getCreateDate(): Date {
        return this.createDate;
    }

    public setGroupName(value: string) {
        this.groupName = value;
    }

    public setCreatorId(value: Account) {
        this.creatorId = value;
    }

    public setCreateDate(value: Date) {
        this.createDate = value;
    }

    public printInformation() {
        console.log("Group Id: " + this.groupId);
        console.log("Group Name: " + this.groupName);
        console.log("Creator Id: " + this.creatorId);
        console.log("Create Date: " + this.createDate);
    }
}

export class GroupAccount {
    private groupId: Group;
    private accountId: Account;
    private joinDate: Date;

    constructor(groupId: Group, accountId: Account) {
        this.groupId = groupId;
        this.accountId = accountId;
        this.joinDate = new Date();
    }

    public getGroupId(): Group {
        return this.groupId;
    }

    public getAccountId(): Account {
        return this.accountId;
    }

    public getJoinDate(): Date {
        return this.joinDate;
    }

    public setGroupId(value: Group) {
        this.groupId = value;
    }

    public setAccountId(value: Account) {
        this.accountId = value;
    }

    public setJoinDate(value: Date) {
        this.joinDate = value;
    }

    public printInformation() {
        console.log("Group Id: " + this.groupId);
        console.log("Account Id: " + this.accountId);
        console.log("Join Date: " + this.joinDate);
    }
}


// ------------------------------------------- String ---------------------------------------------
import * as readline from 'readline';

let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
}); 

export class Exercise1 {
    public question1() {
        rl.question('Nhập vào chuỗi ký tự: ', (string: string) => {
            console.log("số ký tự của chuỗi " + string + " là: " + string.length);
            rl.close();
        }
        )}

    public question2() {
        rl.question('Nhập vào chuỗi thứ nhất: ', (s1: string) => {
            rl.question('Nhập vào chuỗi thứ hai: ', (s2: string) => {
                console.log(s1.concat(s2));
                rl.close();
            })
        })
    }

    public question3() {
        rl.question('Nhập vào tên: ', (name: string) => {
            var firstChar: string = name.charAt(0).toUpperCase();
            var remain: string = name.substring(1);
            console.log("Tên sau khi định dạng là: " + firstChar.concat(remain));
            rl.close();
        })       
    }

    public question4() {
        rl.question('Nhập vào tên: ', (name: string) => {
            for (let i = 0; i < name.length; i++) {
                console.log("Ký tự thứ " + (i+1) + " là: " + name.charAt(i).toUpperCase());
            }
            rl.close();
        })   
    }
    
    public question5() {
        rl.question('Nhập vào họ: ', (firstName: string) => {
            rl.question('Nhập vào tên: ', (lastName: string) => {
                console.log('Họ và tên: ' + firstName.concat(' ').concat(lastName));
                rl.close();
            })
        })
    }

    public question6() {
        rl.question('Nhập vào họ tên: ', (fullName: string) => {
            var words: string[] = fullName.split(' ');
            var firstName: string = words[0];
            var lastName: string = words[words.length - 1];
            var middleName: string = '';
            for (let i = 1; i < (words.length - 1); i++) {
                middleName += words[i] + ' ';
            }
            console.log('Họ là: ' + firstName);
            console.log('Tên đệm là: ' + middleName);
            console.log('Tên là: ' + lastName);
            rl.close();
        })
    }

    public question7() {
        rl.question('Nhập vào họ tên: ', (fullName: string) => {
            fullName = fullName.trim(); //bỏ khoảng trắng 2 đầu
            fullName = fullName.replace(/\s+/g, ' '); //bỏ khoảng trắng ở giữa
            var words: string[] = fullName.split(' ');
            var result: string = ' ';
            for (let i = 0; i < words.length; i++) {
                var firstChar: string = words[i].charAt(0).toUpperCase(); //viết hoa chữ đầu tiên
                var remain: string = words[i].substring(1).toLowerCase();
                var word: string = firstChar.concat(remain); //ghép chữ cái đầu với phần còn lại
                result += word + ' '; //ghép các từ
            }
            console.log('Họ tên sau khi chuẩn hóa là: ' + result);
            rl.close();
        })
    }

    public question8() {
        let groupName: string[] = new Array('Java','JavaScript','.NET','hihiJava');
        for (let i = 0; i < groupName.length; i++) {
            if (groupName[i].search('Java') != -1) {
                console.log(groupName[i]);
            }
        }
    }

    public question9() {
        let groupName: string[] = new Array('Java','JavaScript','.NET');
        for (let i =0; i < groupName.length; i++) {
            if (groupName[i] == 'Java') {
                console.log(groupName[i]);
            }
        }
    }

    public question10(string1: string, string2: string) {
        let reverse: string = '';
        var string1: string = string1;
        var string2: string = string2;
        for (let i = (string1.length - 1); i>=0; i--) {
            var char: string = string1.charAt(i);
            reverse += char;
        }
        if (string2 == reverse) {
            console.log('Đây là chuỗi đảo ngược.');
        } else {
            console.log('Đây không phải là chuỗi đảo ngược.');
        }
    }

    public question11(string: string) {
        var string: string = string;
        var count: number = 0;
        for (let i = 0; i < string.length; i++) {
            if (string.charAt(i).search('a') != -1) {
                count ++;
            }
        }
        console.log("Số lần xuất hiện ký tự a là: " + count);
    }

    public question12(string: string) {
        let reverse: string = '';
        var string: string = string;
        for (let i = (string.length - 1); i>=0; i--) {
            var char: string = string.charAt(i);
            reverse += char;
        }
        console.log("Chuỗi đảo ngược là: " + reverse);
    }

    public question13(string: string) {
        var string: string = string;
        var regex = /[0-9]/gi;
        var result: boolean;
        for (let i = 0; i < string.length; i++) {
            if (string.charAt(i).search(regex) != -1) {
                result = false;
            } else {
                result = true;
            }
        }
        console.log(result);
    }

    public question14(string: string) {
        var string: string = string;
        console.log(string.replace('e','*'));
    }

    public question15(string: string) {
        var words: string[] = string.split(' ');
        var stringReverse: string = '';
        for (let i = (words.length -1); i >= 0; i--) {
            var word: string = words[i];
            stringReverse += word + ' ';
        }
        console.log(words);
        console.log("Chuỗi đảo ngược: " + stringReverse);
    }

    public question16(string: string, n: number) {
        var string: string = string;
        var n: number = n;
        if (string.length % n == 0) {
            for (let i = 0; i < string.length; i += n) {
                console.log(string.substring(i, (i + n)));
            }
        } else {
            console.log("KO");
        }
    }
}