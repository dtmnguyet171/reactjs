"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Exercise2 = void 0;
var Exercise2 = /** @class */ (function () {
    function Exercise2() {
    }
    Exercise2.prototype.question1 = function () {
        var salary1 = 5240.5;
        var salary2 = 10970.055;
        var salaryRound1 = salary1.toFixed();
        var salaryRound2 = salary2.toFixed();
        console.log('Lương Account 1 sau khi làm tròn là: ' + salaryRound1);
        console.log('Lương Account 2 sau khi làm tròn là: ' + salaryRound2);
    };
    Exercise2.prototype.question2 = function () {
        var randomNumber = Math.random() * 89999 + 10000;
        var result = Math.floor(randomNumber);
        console.log('Số ngẫu nhiên là: ' + result);
        return result;
    };
    Exercise2.prototype.question3 = function () {
        var randomNumber = this.question2();
        console.log('Hai số cuối là: ' + randomNumber % 100);
    };
    return Exercise2;
}());
exports.Exercise2 = Exercise2;
