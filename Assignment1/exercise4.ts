export class Exercise4 {
    public question1() {

    }

    public question2(myName) {
        var a = function printName(myName) {
            console.log(myName);
        }
        a(myName);
    }

    public question3(firstName, lastName) {
        function getGreeting(firstName, lastName) {
            function greeting() {
                return firstName + ' ' + lastName;
            }
            return greeting;
        }
        var result = getGreeting(firstName, lastName);
        console.log(result());
    }

    public question4() {
        var timer = function (i) {
            setTimeout(function timer() {
                console.log(i)
            }, i * 1000)
        };

      for (var i = 1; i <= 5; i++) {
            timer(i);
        }
    }

    public question5(x,y) {
        function makeExponentiation(x,y) {
                return Math.pow(x, y);
        }
        var result = makeExponentiation(x, y);
        if (y == 2) {
            console.log('Bình phương của ' + x + ' là: ' + result);
        } else if (y == 0.5) {
            console.log('Căn bậc 2 của ' + x + ' là: ' + result);
        }
    }
}

