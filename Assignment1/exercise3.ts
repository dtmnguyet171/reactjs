export class Exercise3 {
    public question1() {
        var array: string[] = ['a','b','c','d','e'];

        console.log('In array theo index');
        for (let i = 0; i < array.length; i++) {
            console.log(array[i]);
        }

        console.log('In array theo value');
        array.forEach(element => {
            console.log(element);
        });
    }
}