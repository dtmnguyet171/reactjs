"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// ------------------------------------------ DataType -------------------------------------------------
var exercise1_1 = require("./exercise1");
var exercise1_2 = require("./exercise1");
var exercise1_3 = require("./exercise1");
var exercise1_4 = require("./exercise1");
var exercise1_5 = require("./exercise1");
var department1 = new exercise1_1.Department("Sale");
var department2 = new exercise1_1.Department("Marketing");
var position1 = new exercise1_2.Position("Dev");
var position2 = new exercise1_2.Position("Test");
var position3 = new exercise1_2.Position("Scrum Master");
var position4 = new exercise1_2.Position("PM");
var account1 = new exercise1_3.Account("account1@gmail.com", "account1", "Account 1", department1, position2);
var account2 = new exercise1_3.Account("account2@gmail.com", "account2", "Account 2", department2, position4);
var account3 = new exercise1_3.Account("account3@gmail.com", "account3", "Account 3", department1, position1);
var group1 = new exercise1_4.Group("Group 1", account2);
var group2 = new exercise1_4.Group("Group 2", account1);
var group3 = new exercise1_4.Group("Group 3", account3);
var groupAccount1 = new exercise1_5.GroupAccount(group1, account2);
var groupAccount2 = new exercise1_5.GroupAccount(group3, account2);
var groupAccount3 = new exercise1_5.GroupAccount(group1, account3);
// ------------------------------------------- String ---------------------------------------------
var exercise1_6 = require("./exercise1");
var ex1 = new exercise1_6.Exercise1;
// ex1.question1();
// ex1.question2();
// ex1.question3();
// ex1.question4();
// ex1.question5();
// ex1.question6();
// ex1.question7();
// ex1.question8();
// ex1.question9();
// ex1.question10('OK', 'KOK');
// ex1.question11('anh Tu Duc Phuong Nam');
// ex1.question12('Do Thi Minh Nguyet');
// ex1.question13('Nguyet123')
// ex1.question14('Nguyet');
// ex1.question15('Do Thi Minh Nguyet');
// ex1.question16('Do Thi Minh Nguyet', 4);
// ----------------------------------------- Datatype Casting -----------------------------------------
var exercise2_1 = require("./exercise2");
var ex2 = new exercise2_1.Exercise2;
// ex2.question1();
// ex2.question2();
// ex2.question3();
// ------------------------------------------ Flow Control & Operator -------------------------------------
var exercise3_1 = require("./exercise3");
var ex3 = new exercise3_1.Exercise3;
// ex3.question1();
// ------------------------------------------ Scope, Closure --------------------------------------
var exercise4_1 = require("./exercise4");
var ex4 = new exercise4_1.Exercise4;
// ex4.question2('Do Thi Minh Nguyet');
// ex4.question3('Do', 'Nguyet');
// ex4.question4();
// ex4.question5(9, 0.5);
